using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace AuctionSite.Models.Database
{
    public class Package
    {
        [Key]
        public ulong id { get; set; }
        [Required]
        [Display(Name = "Naziv")]
        public string name { get; set; }
        [Required]
        [Display(Name = "Broj tokena")]
        public uint tokens { get; set; }
        [Required]
        [Display(Name = "Cena")]
        public double price { get; set; }

        public ICollection<Order> orders;
    }

    public class PackageConfiguration : IEntityTypeConfiguration<Package>
    {
        public void Configure(EntityTypeBuilder<Package> builder)
        {
            builder.Property(package => package.id).ValueGeneratedOnAdd();

            builder.HasData(
                new Package()
                {
                    id = 1,
                    name = "silver",
                    tokens = 5,
                    price = 3.2
                },
                new Package()
                {
                    id = 2,
                    name = "gold",
                    tokens = 10,
                    price = 6.7
                },
                new Package()
                {
                    id = 3,
                    name = "platinum",
                    tokens = 20,
                    price = 12.99
                }
            );
        }
    }
}