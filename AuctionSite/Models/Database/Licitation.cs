using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuctionSite.Models.Database
{
    public class Licitation
    {
        public ulong idUser { get; set; }
        [Required]
        [Display(Name = "Korisnik")]
        public User user { get; set; }
        public ulong idAuction { get; set; }
        [Required]
        [Display(Name = "Aukcija")]
        public Auction auction { get; set; }
        [Required]
        [Display(Name = "Trenutak licitacije")]
        public string timestamp { get; set; }
    }

    public class LicitationConfiguration : IEntityTypeConfiguration<Licitation>
    {
        public void Configure(EntityTypeBuilder<Licitation> builder)
        {
            builder.HasKey(
                entity => new { entity.idUser, entity.idAuction, entity.timestamp }
            );
        }
    }
}