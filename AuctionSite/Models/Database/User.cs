using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace AuctionSite.Models.Database
{
    public enum Sex
    {
        Male = 0,
        Female = 1
    }

    public class User : IdentityUser<ulong>
    {
        //koje su njegove aukcije, koje aukcije je osvojio, koje tokene je kupio
        //[Key]
        //public ulong id { get; set; }
        [Required]
        [Display(Name = "Ime")]
        public string firstName { get; set; }
        [Required]
        [Display(Name = "Prezime")]
        public string lastName { get; set; }
        [Required]
        [Display(Name = "Pol")]
        public Sex sex { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        [Display(Name = "Korisnicko ime")]
        public string username { get; set; }
        [Required]
        [Display(Name = "Broj tokena")]
        public ulong tokens { get; set; }
        [Required]
        [Display(Name = "Obrisan korisnik")]
        public bool deleted { get; set; }

        public ICollection<Auction> auctions;
        public ICollection<Licitation> licitations;
    }

    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(user => user.Id).ValueGeneratedOnAdd();

            builder.HasData(
                new User()
                {
                    Id = 1,
                    firstName = "Pera",
                    lastName = "Peric",
                    sex = Sex.Male,
                    email = "peraPeric@gmail.com",
                    password = "sifra",
                    username = "pPera",
                    tokens = 5,
                    deleted = false
                },
                new User()
                {
                    Id = 2,
                    firstName = "Mika",
                    lastName = "Mikic",
                    sex = Sex.Female,
                    email = "mikaMikic@gmail.com",
                    password = "sifra",
                    username = "mMika",
                    tokens = 15,
                    deleted = false
                }
            );
        }
    }
}