using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace AuctionSite.Models.Database
{

    public enum AuctionState
    {
        DRAFT = 0,
        READY = 1,
        OPEN = 2,
        SOLD = 3,
        EXPIRED = 4,
        DELETED = 5
    }

    public class Auction
    {
        public Auction()
        {
            state = AuctionState.DRAFT;
        }

        [Key]
        public long id { get; set; }
        [Required]
        [Display(Name = "Naziv")]
        [Remote(controller: "Auction", action: "CheckTitle")]
        public string name { get; set; }
        [Required]
        [Display(Name = "Opis")]
        [Remote(controller: "Auction", action: "CheckDescription")]
        public string description { get; set; }
        [Display(Name = "Slika")]
        [Remote(controller: "Auction", action: "CheckPicture")]
        public byte[] image { get; set; }
        [Required]
        [Display(Name = "Pocetna cena")]
        [Remote(controller: "Auction", action: "CheckStartingPrice")]
        public double startingPrice { get; set; }
        [Required]
        [Display(Name = "Trenutak kreiranja")]
        [Remote(controller: "Auction", action: "CheckCreateDateTime")]
        public string createdTimestamp { get; set; }
        [Required]
        [Display(Name = "Trenutak otvaranja")]
        [Remote(controller: "Auction", action: "CheckOpenDateTime")]
        public string openedTimestamp { get; set; }
        [Required]
        [Display(Name = "Trenutak zatvaranja")]
        [Remote(controller: "Auction", action: "CheckCloseDateTime")]
        public string closedTimestamp { get; set; }
        //default
        [Display(Name = "Stanje")]
        public AuctionState state { get; set; }
        [Required]
        [Display(Name = "Povecanje cene")]
        [Remote(controller: "Auction", action: "CheckIncreasePrice")]
        public double bidPrice { get; set; }
        [Display(Name = "Nova cena")]
        public double newPrice { get; set; }
        [Required]
        [Display(Name = "Vlasnik aukcije")]
        public ulong idOwner { get; set; }
        [Display(Name = "Vlasnik aukcije")]
        public User owner { get; set; }
        [Display(Name = "Licitacije")]
        public ICollection<Licitation> licitations;

        public IList<SelectListItem> getStatuses()
        {
            bool[] activeSates = { false, false, false, false, false, false };
            activeSates[(int)state] = true;
            SelectListItem draft = new SelectListItem("DRAFT", AuctionState.DRAFT.ToString(), activeSates[0]);
            SelectListItem ready = new SelectListItem("DRAFT", AuctionState.READY.ToString(), activeSates[1]);
            SelectListItem open = new SelectListItem("DRAFT", AuctionState.OPEN.ToString(), activeSates[2]);
            SelectListItem sold = new SelectListItem("DRAFT", AuctionState.SOLD.ToString(), activeSates[3]);
            SelectListItem expired = new SelectListItem("DRAFT", AuctionState.EXPIRED.ToString(), activeSates[4]);
            SelectListItem deleted = new SelectListItem("DRAFT", AuctionState.DELETED.ToString(), activeSates[5]);

            IList<SelectListItem> states = new List<SelectListItem>();

            states.Add(draft);
            states.Add(ready);
            states.Add(open);
            states.Add(sold);
            states.Add(expired);
            states.Add(deleted);

            return states;
        }
    }

    public class AuctionConfiguration : IEntityTypeConfiguration<Auction>
    {
        public void Configure(EntityTypeBuilder<Auction> builder)
        {
            builder.Property(auction => auction.id).ValueGeneratedOnAdd();

            builder.HasData(
                new Auction()
                {
                    id = 1,
                    name = "Monitor ",
                    description = "Polovan monitor kraden iz poslovnice MTSa",
                    image = null,
                    startingPrice = 20,
                    state = AuctionState.DRAFT,
                    createdTimestamp = "2020-06-23 10:30:21",
                    openedTimestamp = "2020-06-23 10:30:21",
                    closedTimestamp = "2020-06-23 10:30:21"
                }
            );
        }
    }
}