using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuctionSite.Models.Database
{
    public class Order
    {
        [Key]
        public ulong id { get; set; }
        public ulong idUser { get; set; }
        [Required]
        [Display(Name = "Korisnik")]
        public User user { get; set; }
        [Required]
        public ulong idPackage { get; set; }
        [Display(Name = "Paket")]
        public Package package { get; set; }
        [Required]
        [Display(Name = "Nastala")]
        public string timestampCreated { get; set; }
    }

    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.Property(order => order.id).ValueGeneratedOnAdd();
        }
    }
}