using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuctionSite.Models.Database
{
    public static class UserRoles
    {
        public static IdentityRole administrator = new IdentityRole()
        {
            Name = "Admin",
            NormalizedName = "ADMIN"
        };
        public static IdentityRole user = new IdentityRole()
        {
            Name = "User",
            NormalizedName = "USER"
        };
    }

    public class IdentityRoleConfiguration : IEntityTypeConfiguration<IdentityRole>
    {
        public void configure(EntityTypeBuilder<IdentityRole> builder)
        {
            builder.HasData(
                UserRoles.administrator,
                UserRoles.user
            );
        }
    }
}