using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace AuctionSite.Models.Database
{
    public class AuctionContext : IdentityDbContext<User>
    {
        public AuctionContext(DbContextOptions options) : base(options) { }

        public DbSet<Auction> auctions { get; set; }
        public DbSet<Licitation> licitations { get; set; }
        public DbSet<Package> packages { get; set; }
        public DbSet<Order> orders { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new UserConfiguration());
            builder.ApplyConfiguration(new AuctionConfiguration());
            builder.ApplyConfiguration(new LicitationConfiguration());
            builder.ApplyConfiguration(new PackageConfiguration());
            builder.ApplyConfiguration(new OrderConfiguration());
            builder.ApplyConfiguration(new IdentityRoleConfiguration());
        }
    }
}