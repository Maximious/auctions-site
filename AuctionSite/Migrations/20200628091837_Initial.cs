﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AuctionSite.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "packages",
                columns: table => new
                {
                    id = table.Column<decimal>(nullable: false),
                    name = table.Column<string>(nullable: false),
                    tokens = table.Column<long>(nullable: false),
                    price = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_packages", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    id = table.Column<decimal>(nullable: false),
                    firstName = table.Column<string>(nullable: false),
                    lastName = table.Column<string>(nullable: false),
                    sex = table.Column<int>(nullable: false),
                    email = table.Column<string>(nullable: false),
                    password = table.Column<string>(nullable: false),
                    username = table.Column<string>(nullable: false),
                    tokens = table.Column<decimal>(nullable: false),
                    deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "auctions",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: false),
                    description = table.Column<string>(nullable: false),
                    image = table.Column<byte[]>(nullable: true),
                    startingPrice = table.Column<double>(nullable: false),
                    createdTimestamp = table.Column<string>(nullable: false),
                    openedTimestamp = table.Column<string>(nullable: false),
                    closedTimestamp = table.Column<string>(nullable: false),
                    state = table.Column<int>(nullable: false),
                    bidPrice = table.Column<double>(nullable: false),
                    newPrice = table.Column<double>(nullable: false),
                    idOwner = table.Column<decimal>(nullable: false),
                    ownerid = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_auctions", x => x.id);
                    table.ForeignKey(
                        name: "FK_auctions_users_ownerid",
                        column: x => x.ownerid,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "orders",
                columns: table => new
                {
                    id = table.Column<decimal>(nullable: false),
                    idUser = table.Column<decimal>(nullable: false),
                    userid = table.Column<decimal>(nullable: false),
                    idPackage = table.Column<decimal>(nullable: false),
                    packageid = table.Column<decimal>(nullable: true),
                    timestampCreated = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_orders", x => x.id);
                    table.ForeignKey(
                        name: "FK_orders_packages_packageid",
                        column: x => x.packageid,
                        principalTable: "packages",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_orders_users_userid",
                        column: x => x.userid,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "licitations",
                columns: table => new
                {
                    idUser = table.Column<decimal>(nullable: false),
                    idAuction = table.Column<decimal>(nullable: false),
                    timestamp = table.Column<string>(nullable: false),
                    userid = table.Column<decimal>(nullable: false),
                    auctionid = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_licitations", x => new { x.idUser, x.idAuction, x.timestamp });
                    table.ForeignKey(
                        name: "FK_licitations_auctions_auctionid",
                        column: x => x.auctionid,
                        principalTable: "auctions",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_licitations_users_userid",
                        column: x => x.userid,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "auctions",
                columns: new[] { "id", "bidPrice", "closedTimestamp", "createdTimestamp", "description", "idOwner", "image", "name", "newPrice", "openedTimestamp", "ownerid", "startingPrice", "state" },
                values: new object[] { 1L, 0.0, "2020-06-23 10:30:21", "2020-06-23 10:30:21", "Polovan monitor kraden iz poslovnice MTSa", 0m, null, "Monitor ", 0.0, "2020-06-23 10:30:21", null, 20.0, 0 });

            migrationBuilder.InsertData(
                table: "packages",
                columns: new[] { "id", "name", "price", "tokens" },
                values: new object[,]
                {
                    { 1m, "silver", 3.2000000000000002, 5L },
                    { 2m, "gold", 6.7000000000000002, 10L },
                    { 3m, "platinum", 12.99, 20L }
                });

            migrationBuilder.InsertData(
                table: "users",
                columns: new[] { "id", "deleted", "email", "firstName", "lastName", "password", "sex", "tokens", "username" },
                values: new object[,]
                {
                    { 1m, false, "peraPeric@gmail.com", "Pera", "Peric", "sifra", 0, 5m, "pPera" },
                    { 2m, false, "mikaMikic@gmail.com", "Mika", "Mikic", "sifra", 1, 15m, "mMika" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_auctions_ownerid",
                table: "auctions",
                column: "ownerid");

            migrationBuilder.CreateIndex(
                name: "IX_licitations_auctionid",
                table: "licitations",
                column: "auctionid");

            migrationBuilder.CreateIndex(
                name: "IX_licitations_userid",
                table: "licitations",
                column: "userid");

            migrationBuilder.CreateIndex(
                name: "IX_orders_packageid",
                table: "orders",
                column: "packageid");

            migrationBuilder.CreateIndex(
                name: "IX_orders_userid",
                table: "orders",
                column: "userid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "licitations");

            migrationBuilder.DropTable(
                name: "orders");

            migrationBuilder.DropTable(
                name: "auctions");

            migrationBuilder.DropTable(
                name: "packages");

            migrationBuilder.DropTable(
                name: "users");
        }
    }
}
