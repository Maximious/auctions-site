using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AuctionSite.Models.Database;

namespace AuctionSite.Controllers
{
    public class AdminController : Controller
    {
        private readonly AuctionContext _context;

        public AdminController(AuctionContext context)
        {
            _context = context;
        }

        // GET: User
        public IActionResult Index()
        {
            return View("Views/User/Index.cshtml", _context.Users.ToListAsync());
        }
    }
}