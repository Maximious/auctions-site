using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AuctionSite.Models.Database;

namespace AuctionSite.Controllers
{
    public class LicitationController : Controller
    {
        private readonly AuctionContext _context;

        public LicitationController(AuctionContext context)
        {
            _context = context;
        }

        // GET: Licitation
        public async Task<IActionResult> Index()
        {
            return View(await _context.licitations.ToListAsync());
        }

        // GET: Licitation/Details/5
        public async Task<IActionResult> Details(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var licitation = await _context.licitations
                .FirstOrDefaultAsync(m => m.idUser == id);
            if (licitation == null)
            {
                return NotFound();
            }

            return View(licitation);
        }

        // GET: Licitation/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Licitation/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("idUser,idAuction,timestamp")] Licitation licitation)
        {
            if (ModelState.IsValid)
            {
                _context.Add(licitation);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(licitation);
        }

        // GET: Licitation/Edit/5
        public async Task<IActionResult> Edit(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var licitation = await _context.licitations.FindAsync(id);
            if (licitation == null)
            {
                return NotFound();
            }
            return View(licitation);
        }

        // POST: Licitation/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ulong id, [Bind("idUser,idAuction,timestamp")] Licitation licitation)
        {
            if (id != licitation.idUser)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(licitation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LicitationExists(licitation.idUser))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(licitation);
        }

        // GET: Licitation/Delete/5
        public async Task<IActionResult> Delete(ulong? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var licitation = await _context.licitations
                .FirstOrDefaultAsync(m => m.idUser == id);
            if (licitation == null)
            {
                return NotFound();
            }

            return View(licitation);
        }

        // POST: Licitation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(ulong id)
        {
            var licitation = await _context.licitations.FindAsync(id);
            _context.licitations.Remove(licitation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LicitationExists(ulong id)
        {
            return _context.licitations.Any(e => e.idUser == id);
        }
    }
}
