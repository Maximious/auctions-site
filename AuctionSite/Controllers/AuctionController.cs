using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AuctionSite.Models.Database;

namespace AuctionSite.Controllers
{
    public class AuctionController : Controller
    {
        private readonly AuctionContext _context;

        public AuctionController(AuctionContext context)
        {
            _context = context;
        }

        // GET: Auction
        public async Task<IActionResult> Index()
        {
            //where(author => autgor.name && ...).Any
            return View(await _context.auctions.ToListAsync());
        }

        // GET: Auction/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var auction = await _context.auctions
                .FirstOrDefaultAsync(m => m.id == id);
            if (auction == null)
            {
                return NotFound();
            }

            return View(auction);
        }

        // GET: Auction/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Auction/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,name,description,image,startingPrice,createdTimestamp,openedTimestamp,closedTimestamp,state,bidPrice,newPrice,idOwner")] Auction auction)
        {
            if (ModelState.IsValid)
            {
                //ovde neka provera da li postoji u bazi
                _context.Add(auction);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
                //else Model.State.AddModelError("","Nesto already exists!");
            }
            return View(auction);
        }

        // GET: Auction/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var auction = await _context.auctions.FindAsync(id);
            if (auction == null)
            {
                return NotFound();
            }
            return View(auction);
        }

        // POST: Auction/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("id,name,description,image,startingPrice,createdTimestamp,openedTimestamp,closedTimestamp,state,bidPrice,newPrice,idOwner")] Auction auction)
        {
            if (id != auction.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(auction);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AuctionExists(auction.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(auction);
        }

        // GET: Auction/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var auction = await _context.auctions
                .FirstOrDefaultAsync(m => m.id == id);
            if (auction == null)
            {
                return NotFound();
            }

            return View(auction);
        }

        // POST: Auction/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var auction = await _context.auctions.FindAsync(id);
            _context.auctions.Remove(auction);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AuctionExists(long id)
        {
            return _context.auctions.Any(e => e.id == id);
        }

        public IActionResult CheckStartingPrice(double startingPrice)
        {
            if (startingPrice < 0)
                return Json("Povecanje ne moze biti negativno " + startingPrice);
            else
                return Json(true);
        }

        public IActionResult CheckIncreasePrice(double bidPrice)
        {
            if (bidPrice < 0)
                return Json("Povecanje ne moze biti negativno " + bidPrice);
            else
                return Json(true);
        }

        public IActionResult CheckDescription(string description)
        {
            if (description.Length > 1500)
                return Json("Predugacak opis" + description.Length);
            else
                return Json(true);
        }

        public IActionResult CheckTitle(string name)
        {
            if (name.Length > 150)
                return Json("Predugacak naslov" + name.Length);
            else
                return Json(true);
        }

        public IActionResult CheckCreateDateTime(string createdTimestamp)
        {
            if (createdTimestamp.Length > 15)
                return Json("Predugacak datetime" + createdTimestamp.Length);
            else
                return Json(true);
        }

        public IActionResult CheckOpenDateTime(string openedTimestamp)
        {
            if (openedTimestamp.Length > 15)
                return Json("Predugacak datetime" + openedTimestamp.Length);
            else
                return Json(true);
        }

        public IActionResult CheckCloseDateTime(string closedTimestamp)
        {
            if (closedTimestamp.Length > 15)
                return Json("Predugacak datetime" + closedTimestamp.Length);
            else
                return Json(true);
        }

        //CheckDateTime //CheckPicture 
    }
}

