using Microsoft.AspNetCore.Mvc;
using AuctionSite.Models.Database;

namespace AuctionSite.Controllers
{
    public class RegistrationController : Controller
    {
        private readonly AuctionContext _context;

        public RegistrationController(AuctionContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
