using System.Collections.Generic;
using BidR.Models.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using System;
using Microsoft.EntityFrameworkCore;

namespace BidR.Controllers
{
    [Authorize(Roles = "User")]
    public class TokenController : Controller
    {
        private BidRContext context;
        private UserManager<User> userManager;

        public TokenController(BidRContext context, UserManager<User> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }

        public IActionResult Index()
        {
            List<Package> packages = this.context.packages.ToList();
            return View(packages);
        }

        [HttpGet]
        public IActionResult BuyPackage(int Id)
        {
            Package package = this.context.packages.Where(package => package.Id == Id).First();
            return View(package);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> BuyPackageAsync(int Id, int? dummy)//dummy samo zbog potpisa fje
        {
            Package package = context.packages.Find(Id);
            User user = await userManager.GetUserAsync(User);
            Order order = new Order
            {
                UserId = user.Id,
                User = user,
                PackageId = package.Id,
                Package = package,
                timestampCreated = DateTime.Now
            };
            user.tokens = user.tokens + package.tokens;

            try
            {
                await this.context.orders.AddAsync(order);
                await this.context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                foreach (var entry in ex.Entries)
                {//check this out
                    var databaseValues = entry.GetDatabaseValues();
                    var newUser = (User)databaseValues.ToObject();
                    databaseValues["tokens"] = newUser.tokens + package.tokens;
                    entry.OriginalValues.SetValues(databaseValues);
                }
            }

            return Json("Done");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> OrderList()
        {
            User user = await userManager.GetUserAsync(User);
            List<Order> orders = await context.orders.Where(order => order.UserId == user.Id).OrderByDescending(order => order.timestampCreated).Include(order => order.Package).ToListAsync();
            return Json(orders);
        }
    }
}