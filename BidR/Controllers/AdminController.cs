using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BidR.Models.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BidR.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdminController : Controller
    {
        private BidRContext context;
        private UserManager<User> userManager;
        private SignInManager<User> signInManager;

        public AdminController(BidRContext context, UserManager<User> userManager,
            SignInManager<User> signInManager)
        {
            this.context = context;
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public IActionResult Index()
        {
            return this.Auctions();
        }

        public IActionResult Users()
        {
            List<User> users = this.context.Users.Where(user => user.UserName != "administrator" && user.deleted == false).ToList();
            return View(users);
        }

        public IActionResult Auctions()
        {
            List<Auction> auctions = this.context.auctions.Where(auction => auction.state == AuctionState.DRAFT).Include(auction => auction.User).ToList();
            return View(auctions);
        }

        public async Task<IActionResult> DeleteUser(string username)
        {
            if (username == null)
            {
                return NotFound();
            }

            var user = await context.Users
                .FirstOrDefaultAsync(m => m.UserName == username);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string Id)
        {
            var user = await context.Users.FindAsync(Id);
            user.deleted = true;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                foreach (var entry in ex.Entries)
                {
                    var databaseValues = entry.GetDatabaseValues();
                    databaseValues["deleted"] = 1;
                    entry.OriginalValues.SetValues(databaseValues);
                }
            }

            //update auctions
            List<Auction> auctions = this.context.auctions.Where(auction => auction.UserId == Id).ToList();

            foreach (Auction auction in auctions)
            {
                auction.state = AuctionState.EXPIRED;
            }

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                foreach (var entry in ex.Entries)
                {
                    var databaseValues = entry.GetDatabaseValues();
                    databaseValues["state"] = AuctionState.EXPIRED;
                    entry.OriginalValues.SetValues(databaseValues);
                }
            }

            return RedirectToAction(nameof(Users));
        }

        public async Task<IActionResult> AuctionPreview(int? Id)
        {
            if (Id == null)
            {
                return NotFound();
            }

            var auction = await context.auctions.Include(auction => auction.User)
                .FirstOrDefaultAsync(m => m.Id == Id);
            if (auction == null || auction.state != AuctionState.DRAFT)
            {
                return NotFound();
            }

            return View(auction);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AuctionConfirmed(int Id)
        {
            var auction = await context.auctions.FindAsync(Id);
            auction.state = AuctionState.READY;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                foreach (var entry in ex.Entries)
                {
                    var databaseValues = entry.GetDatabaseValues();
                    databaseValues["state"] = AuctionState.READY;
                    entry.OriginalValues.SetValues(databaseValues);
                }
            }

            return RedirectToAction(nameof(Auctions));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AuctionDeleted(int Id)
        {
            var auction = await context.auctions.FindAsync(Id);
            auction.state = AuctionState.DELETED;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                foreach (var entry in ex.Entries)
                {
                    var databaseValues = entry.GetDatabaseValues();
                    databaseValues["state"] = AuctionState.DELETED;
                    entry.OriginalValues.SetValues(databaseValues);
                }
            }

            return RedirectToAction(nameof(Auctions));
        }
    }
}