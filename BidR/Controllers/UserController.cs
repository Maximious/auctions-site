using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using BidR.Models.Database;
using BidR.Models.View;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BidR.Controllers
{
    public class UserController : Controller
    {
        private BidRContext context;
        private UserManager<User> userManager;
        private IMapper mapper;
        private SignInManager<User> signInManager;

        public UserController(BidRContext context, UserManager<User> userManager,
            IMapper mapper, SignInManager<User> signInManager)
        {
            this.context = context;
            this.userManager = userManager;
            this.mapper = mapper;
            this.signInManager = signInManager;
        }

        public IActionResult Register()
        {
            if (signInManager.IsSignedIn(User))
                return RedirectToRoute(new { controller = "Auction", action = "Index" });
            return View();
        }

        public IActionResult uniqueEmail(string email)
        {
            bool exists = this.context.Users.Where(user => user.Email == email).Any();
            if (exists)
            {
                return Json("Email taken!");
            }
            else
            {
                return Json(true);
            }
        }

        public IActionResult passwordOk(string password)
        {
            if (Regex.Matches(password, @"[a-zA-Z]").Count == 0)
            {
                return Json("At least one character required!");
            }
            if (Regex.Matches(password, @"[0-9]").Count == 0)
            {
                return Json("At least one number required!");
            }
            if (password.Length < 8)
            {
                return Json("Password must be at leas 8 characters long!");
            }
            return Json(true);
        }

        public IActionResult correctUsername(string username)
        {
            if (username.Length < 6)
            {
                return Json("The field Username must be a string with a minimum length of 6.");
            }
            bool exists = this.context.Users.Where(user => user.UserName == username).Any();
            if (exists)
            {
                return Json("Username taken!");
            }
            else
            {
                return Json(true);
            }
        }

        public async Task<IActionResult> uniqueEmailSettings(string email)
        {
            User user = await userManager.GetUserAsync(User);
            if (email != user.Email)
            {
                bool exists = this.context.Users.Where(user => user.Email == email).Any();

                if (exists)
                {
                    return Json("Email taken!");
                }
            }

            return Json(true);
        }

        public bool firstLastNameCheck(string name)
        {
            if (Regex.Matches(name, @"[a-zA-Z''-'\s]").Count != name.Length)
            {
                return false;

            }
            return true;
        }

        public async Task<IActionResult> correctUsernameSettings(string username)
        {
            User user = await userManager.GetUserAsync(User);

            if (user.UserName != username)
            {
                if (username.Length < 6)
                {
                    return Json("The field Username must be a string with a minimum length of 6.");
                }
                bool exists = this.context.Users.Where(user => user.UserName == username).Any();
                if (exists)
                {
                    return Json("Username taken!");
                }
            }

            return Json(true);

        }

        private bool CheckForErrors(RegisterModel model)
        {
            bool error = false;
            if (!firstLastNameCheck(model.firstName) || !firstLastNameCheck(model.lastName))
            {
                ModelState.AddModelError("", "Only characters and spaces allowed for first and last name.");
                error = true;
            }
            if (model.gender != Gender.Male && model.gender != Gender.Female)
            {
                ModelState.AddModelError("", "Gender not recognised!");
                error = true;
            }
            if (Regex.Matches(model.email, @"^.*[a-zA-z]+@[a-zA-z]+\.com$").Count == 0)
            {
                ModelState.AddModelError("", "Email format not valid!");
                error = true;
            }
            if (model.username.Length < 6)
            {
                ModelState.AddModelError("", "The field Username must be a string with a minimum length of 6.");
                error = true;
            }
            bool exists = this.context.Users.Where(user => user.UserName == model.username).Any();
            if (exists)
            {
                ModelState.AddModelError("", "Username already taken!");
                error = true;
            }
            if (Regex.Matches(model.password, @"[a-zA-Z]").Count == 0)
            {
                ModelState.AddModelError("", "Password requires character!");
                error = true;
            }
            if (Regex.Matches(model.password, @"[0-9]").Count == 0)
            {
                ModelState.AddModelError("", "Password requires number!");
                error = true;
            }
            if (model.password.Length < 6)
            {
                ModelState.AddModelError("", "Username already taken!");
                error = true;
            }
            return error;
        }

        private async Task<bool> CheckForErrors(EditModel model)
        {
            bool error = false;
            if (!firstLastNameCheck(model.firstName) || !firstLastNameCheck(model.lastName))
            {
                ModelState.AddModelError("", "Only characters and spaces allowed for first and last name.");
                error = true;
            }
            if (model.gender != Gender.Male && model.gender != Gender.Female)
            {
                ModelState.AddModelError("", "Gender not recognised!");
                error = true;
            }

            User user = await userManager.GetUserAsync(User);
            if (user.Email != model.email)
            {
                if (Regex.Matches(model.email, @"^.*[a-zA-z]+@[a-zA-z]+\.com$").Count == 0)
                {
                    ModelState.AddModelError("", "Email format not valid!");
                    error = true;
                }
                User sameMailUser = this.context.Users.Where(user => user.Email == model.email).FirstOrDefault();
                if (sameMailUser != null)
                {
                    ModelState.AddModelError("", "Email already exists!");
                    error = true;
                }
            }

            if (user.UserName != model.username)
            {
                bool exists = this.context.Users.Where(user => user.UserName == model.username).Any();
                if (exists)
                {
                    ModelState.AddModelError("", "Username already taken!");
                    error = true;
                }
                if (model.username.Length < 6)
                {
                    ModelState.AddModelError("", "The field Username must be a string with a minimum length of 6.");
                    error = true;
                }
            }

            if (model.password != null)
            {
                if (Regex.Matches(model.password, @"[a-zA-Z]").Count == 0)
                {
                    ModelState.AddModelError("", "Password requires character!");
                    error = true;
                }
                if (Regex.Matches(model.password, @"[0-9]").Count == 0)
                {
                    ModelState.AddModelError("", "Password requires number!");
                    error = true;
                }
                if (model.password.Length < 6)
                {
                    ModelState.AddModelError("", "Username already taken!");
                    error = true;
                }
            }
            return error;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //server data check            
            if (CheckForErrors(model))
                return View(model);

            User user = this.mapper.Map<User>(model);
            user.deleted = false;
            user.tokens = 5;

            IdentityResult result = await this.userManager.CreateAsync(user, model.password);

            if (!result.Succeeded)
            {
                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }

                return View(model);
            }

            result = await this.userManager.AddToRoleAsync(user, Roles.user.Name);

            if (!result.Succeeded)
            {
                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }

                return View(model);
            }

            return RedirectToRoute(new { controller = "User", action = "LogIn" });
        }

        public IActionResult LogIn(string returnUrl)
        {
            if (signInManager.IsSignedIn(User))
                return RedirectToRoute(new { controller = "Auction", action = "Index" });
            LogInModel model = new LogInModel();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogIn(LogInModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            User loggedUser = this.context.Users.Where(user => user.UserName == model.username).FirstOrDefault();

            var result = await this.signInManager.PasswordSignInAsync(model.username, model.password, false, false);

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Username or password not valid!");

                return View(model);
            }

            if (loggedUser == null)
                return RedirectToRoute(new { controller = "Auction", action = "Index" });

            if (loggedUser.deleted)
            {
                ModelState.AddModelError("", "Account has been deleted!");
                return View(model);
            }

            var roleId = context.UserRoles.Where(role => role.UserId == loggedUser.Id).FirstOrDefault();
            var roleName = context.Roles.Where(role => role.Id == roleId.RoleId).FirstOrDefault();

            if (roleName.Name == "Administrator")
                return RedirectToRoute(new { controller = "Admin", action = "Auctions" });
            else
                return RedirectToRoute(new { controller = "Auction", action = "Index" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User, Administrator")]
        public async Task<IActionResult> LogOut()
        {
            await this.signInManager.SignOutAsync();
            return RedirectToRoute(new { controller = "Auction", action = "Index" });
        }

        [Authorize(Roles = "User,Administrator")]
        public async Task<IActionResult> Settings()
        {
            User user = await userManager.GetUserAsync(User);
            EditModel eModel = new EditModel
            {
                firstName = user.firstName,
                lastName = user.lastName,
                email = user.Email,
                gender = user.gender,
                username = user.UserName
            };
            return View(eModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User,Administrator")]
        public async Task<IActionResult> Settings(EditModel eModel)
        {
            if (!ModelState.IsValid)
            {
                return View(eModel);
            }

            //server data check            
            if (await CheckForErrors(eModel))
                return View(eModel);

            User user = await userManager.GetUserAsync(User);

            user.firstName = eModel.firstName;
            user.lastName = eModel.lastName;
            user.Email = eModel.email;
            user.gender = eModel.gender;
            user.UserName = eModel.username;
            if (eModel.password != null && eModel.password.Length > 0)
            {
                var token = await userManager.GeneratePasswordResetTokenAsync(user);
                var result = await userManager.ResetPasswordAsync(user, token, eModel.password);
            }

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                ModelState.AddModelError("", "Some values have been changed!");
                return View("Settings", eModel);
            }

            return RedirectToRoute(new { controller = "Auction", action = "Index" });
        }
    }
}

