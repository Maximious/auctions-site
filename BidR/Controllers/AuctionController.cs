using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BidR.Models.Database;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BidR.Models.View;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using cloudscribe.Pagination.Models;
using Microsoft.AspNetCore.Http;

namespace BidR.Controllers
{
    public class AuctionController : Controller
    {
        private IMapper mapper;
        private readonly BidRContext _context;
        private UserManager<User> userManager;
        private SignInManager<User> signInManager;

        public AuctionController(BidRContext context, UserManager<User> userManager, IMapper mapper, SignInManager<User> signInManager)
        {
            _context = context;
            this.userManager = userManager;
            this.mapper = mapper;
            this.signInManager = signInManager;
        }

        private bool isAdmin()
        {
            User loggedUsers = this.userManager.GetUserAsync(base.User).Result;

            if (loggedUsers != null)
            {
                IdentityUserRole<String> roleId = _context.UserRoles.Where(role => role.UserId == loggedUsers.Id).FirstOrDefault();
                IdentityRole roleName = _context.Roles.Where(role => role.Id == roleId.RoleId).FirstOrDefault();

                if (roleName.Name == "Administrator")
                {
                    return true;
                }
            }
            return false;
        }

        private static bool IsAjax(HttpRequest request, string httpVerb = "")
        {
            if (request == null)
            {
                throw new ArgumentNullException("Request object is Null.");
            }

            if (!string.IsNullOrEmpty(httpVerb))
            {
                if (request.Method != httpVerb)
                {
                    return false;
                }
            }

            if (request.Headers != null)
            {
                return request.Headers["X-Requested-With"] == "XMLHttpRequest";
            }

            return false;
        }

        public IActionResult Index(string name, int? priceLow, int? priceHigh, int? auctionState, int pageNumber = 1, int pageSize = 12)
        {
            if (isAdmin())
                return RedirectToRoute(new { controller = "Admin", action = "Auctions" });

            AuctionState state = AuctionState.OPEN;

            if (auctionState.HasValue)
                state = (AuctionState)Enum.ToObject(typeof(AuctionState), auctionState);

            int skipAuctions = (pageSize * pageNumber) - pageSize;

            var auctions = this._context.auctions.Where(auction => auction.state == state);

            if (name != null && name != "")
            {
                Console.WriteLine("name set");
                auctions = auctions.Where(auction => auction.name.Contains(name));
            }
            if (priceLow.HasValue)
            {
                Console.WriteLine("price low set");
                auctions = auctions.Where(auction => auction.newPrice >= priceLow);
            }
            if (priceHigh.HasValue)
            {
                Console.WriteLine("price high set");
                auctions = auctions.Where(auction => auction.newPrice <= priceHigh);
            }

            var totalItems = auctions.Count();

            auctions = auctions.OrderByDescending(auction => auction.createdTimestamp).Skip(skipAuctions).Take(pageSize).Include(auction => auction.User).Include(auction => auction.Winner);

            var result = new PagedResult<Auction>
            {
                Data = auctions.AsNoTracking().ToList(),
                TotalItems = totalItems,
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            ViewData["Title"] = "Auctions";
            ViewData["Controller"] = "Auction";
            ViewData["Action"] = "Index";

            if (IsAjax(Request))
            {
                return PartialView("_AuctionGrid", result);
            }

            return View(result);
        }

        [Authorize(Roles = "User")]
        public IActionResult MyAuctions(string name, int? priceLow, int? priceHigh, int? auctionState, int pageNumber = 1, int pageSize = 12)
        {
            AuctionState state = AuctionState.OPEN;
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            int skipAuctions = (pageSize * pageNumber) - pageSize;

            var auctions = this._context.auctions.Where(auction => auction.UserId == userId);

            if (auctionState < 6 && auctionState >= 0)
            {
                state = (AuctionState)Enum.ToObject(typeof(AuctionState), auctionState);
                auctions = auctions.Where(auction => auction.state == state);
            }
            if (name != null && name != "")
            {
                auctions = auctions.Where(auction => auction.name.Contains(name));
            }
            if (priceLow.HasValue)
            {
                auctions = auctions.Where(auction => auction.newPrice >= priceLow);
            }
            if (priceHigh.HasValue)
            {
                auctions = auctions.Where(auction => auction.newPrice <= priceHigh);
            }

            var totalItems = auctions.Count();

            auctions = auctions.OrderByDescending(auction => auction.createdTimestamp).Skip(skipAuctions).Take(pageSize).Include(auction => auction.User).Include(auction => auction.Winner);

            var result = new PagedResult<Auction>
            {
                Data = auctions.AsNoTracking().ToList(),
                TotalItems = totalItems,
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            ViewData["Title"] = "My auctions";
            ViewData["Controller"] = "Auction";
            ViewData["Action"] = "MyAuctions";

            if (IsAjax(Request))
            {
                return PartialView("_AuctionGrid", result);
            }

            return View(result);
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> AuctionsWon(string name, int? priceLow, int? priceHigh, int? auctionState, int pageNumber = 1, int pageSize = 12)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            List<Auction> auctions = await this._context.auctions.Where(auction => auction.WinnerId == userId && auction.state == AuctionState.SOLD).Include(auction => auction.User).ToListAsync();

            var result = new PagedResult<Auction>
            {
                Data = auctions,
                TotalItems = auctions.Count(),
                PageNumber = pageNumber,
                PageSize = pageSize
            };

            ViewData["Title"] = "Auctions";
            ViewData["Controller"] = "Auction";
            ViewData["Action"] = "Index";

            if (IsAjax(Request))
            {
                return PartialView("_AuctionGrid", result);
            }

            return View(result);
        }

        private bool CheckForErrors(AuctionModel model)
        {
            bool error = false;
            if (model.name.Length > 150)
            {
                ModelState.AddModelError("", "Name too long!!!");
                error = true;
            }
            else if (model.name.Length < 5)
            {
                ModelState.AddModelError("", "Name too short!!!");
                error = true;
            }
            if (model.description.Length > 1500)
            {
                ModelState.AddModelError("", "Description too long!!!");
                error = true;
            }
            else if (model.description.Length < 50)
            {
                ModelState.AddModelError("", "Description too short!!!");
                error = true;
            }
            if (model.startingPrice < 0)
            {
                ModelState.AddModelError("", "Starting price cannot be negative!!!");
                error = true;
            }
            if (model.bidPrice < 0)
            {
                ModelState.AddModelError("", "Bid price cannot be negative!!!");
                error = true;
            }

            DateTime now = DateTime.Now;
            if (model.openedTimestamp <= now)
            {
                ModelState.AddModelError("", "Cannot start auction in the past!!!");
                error = true;
            }
            if (model.openedTimestamp >= model.closedTimestamp)
            {
                ModelState.AddModelError("", "Cannot end auction before opening it!!!");
                error = true;
            }
            if (model.photo != null && !string.Equals(model.photo.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) && !string.Equals(model.photo.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) && !string.Equals(model.photo.ContentType, "image/png", StringComparison.OrdinalIgnoreCase))
            {
                ModelState.AddModelError("", "Wrong image format!!!");
                error = true;
            }
            return error;
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> AddAuctionAsync()
        {
            AuctionModel model = new AuctionModel();
            model.state = AuctionState.DRAFT;
            model.createdTimestamp = DateTime.Now;
            model.UserId = (await userManager.GetUserAsync(base.User)).Id;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> AddAuctionAsync(AuctionModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //server validation
            if (CheckForErrors(model))
                return View(model);

            Auction auction = this.mapper.Map<Auction>(model);
            auction.newPrice = auction.startingPrice;
            auction.Winner = null;
            auction.WinnerId = null;

            using (BinaryReader reader = new BinaryReader(model.photo.OpenReadStream()))
            {
                auction.image = reader.ReadBytes(Convert.ToInt32(reader.BaseStream.Length));
            }

            await this._context.auctions.AddAsync(auction);
            await this._context.SaveChangesAsync();

            return RedirectToRoute(new { controller = "Auction", action = "Index" });
        }

        //[Authorize(Roles = "User")]
        public async Task<IActionResult> AuctionDetails(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var auction = await _context.auctions.Where(m => m.Id == id).Include(auction => auction.Winner).Include(auction => auction.User).FirstAsync();

            if (auction == null)
            {
                return NotFound();
            }

            if (auction.state == AuctionState.DELETED || auction.state == AuctionState.EXPIRED
                || auction.state == AuctionState.DRAFT || auction.state == AuctionState.READY)
            {
                User user = await userManager.GetUserAsync(User);
                if (user == null || user.Id != auction.UserId)
                {
                    return RedirectToRoute(new { controller = "Auction", action = "Index" });
                }
            }
            else if (auction.state == AuctionState.SOLD)
            {
                User user = await userManager.GetUserAsync(User);
                if (user == null || (user.Id != auction.UserId && user.Id != auction.WinnerId))
                {
                    return RedirectToRoute(new { controller = "Auction", action = "Index" });
                }
            }

            return View(auction);
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var auction = await _context.auctions.FindAsync(id);
            User user = await userManager.GetUserAsync(User);

            if (auction == null || auction.state != AuctionState.DRAFT || auction.UserId != user.Id)
            {
                return RedirectToRoute(new { controller = "Auction", action = "Index" });
            }

            EditAuctionModel eAModel = new EditAuctionModel
            {
                name = auction.name,
                description = auction.description,
                image = auction.image,
                startingPrice = auction.startingPrice,
                openedTimestamp = auction.openedTimestamp,
                closedTimestamp = auction.closedTimestamp,
                bidPrice = auction.bidPrice
            };

            return View(eAModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> Edit(EditAuctionModel auctionModel)
        {
            if (auctionModel == null)
            {
                return NotFound();
            }

            var auction = await _context.auctions.FindAsync(auctionModel.Id);

            if (ModelState.IsValid)
            {
                //server validation
                if (CheckForErrors(new AuctionModel
                {
                    name = auctionModel.name,
                    description = auctionModel.description,
                    startingPrice = auctionModel.startingPrice,
                    bidPrice = auctionModel.bidPrice,
                    openedTimestamp = auctionModel.openedTimestamp,
                    closedTimestamp = auctionModel.closedTimestamp,
                    photo = auctionModel.photo
                }))
                    return View(auctionModel);

                try
                {
                    auction.name = auctionModel.name;
                    auction.description = auctionModel.description;
                    auction.startingPrice = auctionModel.startingPrice;
                    auction.newPrice = auctionModel.startingPrice;
                    auction.bidPrice = auctionModel.bidPrice;
                    auction.openedTimestamp = auctionModel.openedTimestamp;
                    auction.closedTimestamp = auctionModel.closedTimestamp;

                    if (auctionModel.photo != null)
                    {
                        using (BinaryReader reader = new BinaryReader(auctionModel.photo.OpenReadStream()))
                        {
                            auction.image = reader.ReadBytes(Convert.ToInt32(reader.BaseStream.Length));
                        }
                    }
                    _context.Update(auction);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AuctionExists(auctionModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        var newAuction = await this._context.auctions.FindAsync(auctionModel.Id);
                        ModelState.AddModelError("", "Some values have been changed!");

                        auctionModel.name = newAuction.name;
                        auctionModel.description = newAuction.description;
                        auctionModel.image = newAuction.image;
                        auctionModel.startingPrice = newAuction.startingPrice;
                        auctionModel.createdTimestamp = newAuction.createdTimestamp;
                        auctionModel.openedTimestamp = newAuction.openedTimestamp;
                        auctionModel.closedTimestamp = newAuction.closedTimestamp;
                        auctionModel.bidPrice = newAuction.bidPrice;

                        return View(auctionModel);
                    }
                }
                return RedirectToAction(nameof(MyAuctions));
            }

            return View(auctionModel);
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var auction = await _context.auctions.FindAsync(id);
            User user = await userManager.GetUserAsync(User);

            if (auction == null || auction.state != AuctionState.DRAFT || auction.UserId != user.Id)
            {
                return RedirectToRoute(new { controller = "Auction", action = "Index" });
            }

            EditAuctionModel eAModel = new EditAuctionModel
            {
                name = auction.name,
                description = auction.description,
                image = auction.image,
                startingPrice = auction.startingPrice,
                createdTimestamp = auction.createdTimestamp,
                openedTimestamp = auction.openedTimestamp,
                closedTimestamp = auction.closedTimestamp,
                bidPrice = auction.bidPrice
            };

            return View(eAModel);
        }

        [Authorize(Roles = "User")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var auction = await _context.auctions.FindAsync(id);
            auction.state = AuctionState.DELETED;

            try
            {
                _context.auctions.Update(auction);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                foreach (var entry in ex.Entries)
                {
                    var databaseValues = entry.GetDatabaseValues();
                    databaseValues["state"] = AuctionState.DELETED;
                    entry.OriginalValues.SetValues(databaseValues);
                }
            }


            return RedirectToAction(nameof(MyAuctions));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User")]
        public async Task<IActionResult> Bid(int id)
        {
            //ako licitacija traje jos 10s da se obnovi
            User user = await userManager.GetUserAsync(User);
            if (user.tokens <= 0)
                return Json("failed");
            else
                user.tokens--;

            var auction = await _context.auctions.FindAsync(id);
            auction.newPrice = auction.newPrice + auction.bidPrice;
            auction.Winner = user;
            auction.WinnerId = user.Id;

            DateTime localDate = DateTime.Now;
            TimeSpan timeSpan = auction.closedTimestamp.Subtract(localDate);
            TimeSpan tenSec = new TimeSpan(0, 0, 10);

            if (timeSpan <= tenSec)
            {
                auction.closedTimestamp = localDate + tenSec;
            }

            Licitation licitation = new Licitation
            {
                UserId = user.Id,
                User = user,
                AuctionId = auction.Id,
                Auction = auction,
                timestamp = DateTime.Now
            };

            try
            {
                _context.auctions.Update(auction);
                await _context.licitations.AddAsync(licitation);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                foreach (var entry in ex.Entries)
                {
                    if (entry.Entity is Auction)
                    {
                        var databaseValues = entry.GetDatabaseValues();

                        return Json((Auction)databaseValues.ToObject());
                    }
                    else
                    {
                        throw new NotSupportedException(
                            "Don't know how to handle concurrency conflicts for "
                            + entry.Metadata.Name);
                    }
                }
            }

            return Json(licitation);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Authorize(Roles = "User")]
        public async Task<IActionResult> AuctionLicitations(int id)
        {
            var licitations = await _context.licitations.Where(licitation => licitation.AuctionId == id).Include(licitation => licitation.User).Include(licitation => licitation.Auction).OrderByDescending(licitations => licitations.timestamp).ToListAsync();

            return Json(licitations);
        }

        private bool AuctionExists(int id)
        {
            return _context.auctions.Any(e => e.Id == id);
        }

        public IActionResult AuctionStates()
        {
            var states = Auction.getStatuses();
            return Json(states);
        }

        //real time verification functions
        public IActionResult CheckStartingPrice(double startingPrice)
        {
            if (startingPrice < 0)
                return Json("Starting price cannot be negative!!!");
            else
                return Json(true);
        }

        public IActionResult CheckIncreasePrice(double bidPrice)
        {
            if (bidPrice < 0)
                return Json("Bid price cannot be negative!!!");
            else
                return Json(true);
        }

        public IActionResult CheckDescription(string description)
        {
            if (description.Length > 1500)
                return Json("Description too long!!!");
            else if (description.Length < 50)
                return Json("Description too short!!!");
            else
                return Json(true);
        }

        public IActionResult CheckImage(IFormFile photo)
        {
            if (!string.Equals(photo.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) && !string.Equals(photo.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) && !string.Equals(photo.ContentType, "image/png", StringComparison.OrdinalIgnoreCase))
            {
                return Json("Wrong image format!!!");
            }
            return Json(true);
        }

        public IActionResult CheckTitle(string name)
        {
            if (name.Length > 150)
                return Json("Name too long!!!");
            else if (name.Length < 5)
                return Json("Name too short!!!");
            else
                return Json(true);
        }

        public IActionResult CheckOpenDateTime(DateTime openedTimestamp)
        {
            DateTime now = DateTime.Now;
            if (openedTimestamp <= now)
                return Json("Cannot start auction in the past!!!");
            return Json(true);
        }

        public IActionResult CheckCloseDateTime(DateTime closedTimestamp)
        {
            DateTime now = DateTime.Now;
            if (closedTimestamp <= now)
                return Json("Cannot end auction in the past!!!");
            return Json(true);
        }


        //signalR
        public async Task<IActionResult> GetAuctionDetails(int auctionId)
        {
            User user = await userManager.GetUserAsync(User);
            var auction = await _context.auctions.Where(m => m.Id == auctionId).Include(auction => auction.Winner).FirstAsync();
            //if (user.Id != auction.WinnerId)
            //return Json("failed");
            return Json(auction);
        }
    }
}

