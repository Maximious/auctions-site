using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace BidR.Hubs
{
    public class AuctionsHub : Hub
    {
        //private static List<string> oldGroups = new List<string>();

        public async Task AddToGroup(List<string> groupNames)
        {
            /*
            foreach (string group in oldGroups)
            {
                await base.Groups.RemoveFromGroupAsync(base.Context.ConnectionId, group);
            }*/

            foreach (string group in groupNames)
            {
                await base.Groups.AddToGroupAsync(base.Context.ConnectionId, group);
            }

            //oldGroups = groupNames;
        }

        public async Task AuctionUpdated(string groupName)
        {
            await base.Clients.Group(groupName).SendAsync("UpdateAuction", groupName);
        }
    }
}