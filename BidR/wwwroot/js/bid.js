var connection = new signalR.HubConnectionBuilder().withUrl("/update").build();

function handleError(error) {
	alert(error);
}

connection
	.start()
	.then(function () {
		let auctionsId = $(".auctionId")
			.map((_, el) => el.value)
			.get();
		connection.invoke("AddToGroup", auctionsId).catch(handleError);
	})
	.catch(handleError);

connection.on("UpdateAuction", function (id) {
	$.ajax({
		type: "POST",
		url: "/Auction/GetAuctionDetails",
		data: {
			auctionId: id,
			//__RequestVerificationToken: verificationToken,
		},
		success: function (response) {
			let changePrice = "#price_" + response.id;
			let changeBidder = "#bidder_" + response.id;

			$(changePrice).css("color", "red");
			$(changeBidder).css("color", "red");

			$(changePrice).text(
				"Price: " +
					Math.round((response.newPrice + Number.EPSILON) * 100) /
						100 +
					"$"
			);
			$(changeBidder).text(response.winner.userName);

			setTimeout(function () {
				$(changePrice).css("color", "black");
				$(changeBidder).css("color", "black");
			}, 500);
		},
		error: function (response) {
			alert(response);
		},
	});
});

$(document).ready(function () {
	$(".bidButton").click(function () {
		let tokens = parseInt($("#availableTokens").text());
		if (tokens == 0) return;
		let userName = $("#userName").val();

		let id = parseInt(this.id.split(" ")[1]);
		let formB = $("#__AjaxAntiForgeryForm");
		let tokenB = $('input[name="__RequestVerificationToken"]', formB).val();

		$.ajax({
			url: "https://localhost:5001/Auction/Bid",
			type: "POST",
			data: {
				__RequestVerificationToken: tokenB,
				id: id,
			},
			success: function (result) {
				connection.invoke("AuctionUpdated", "" + id).catch(handleError);
				let elementId = "#bidder_" + result.auctionId;
				let priceId = "#price_" + result.auctionId;
				$(elementId).css("color", "red");
				$(priceId).css("color", "red");
				$(elementId).text(result.user.userName);
				setTimeout(function () {
					$(elementId).css("color", "black");
					$(priceId).css("color", "black");
				}, 500);
				$(priceId).text(
					"Price: " +
						Math.round(
							(result.auction.newPrice + Number.EPSILON) * 100
						) /
							100 +
						"$"
				);
				if (result.user.userName == userName) {
					tokens--;
					$("#availableTokens").text(tokens);
				}
			},
		});
	});
});
