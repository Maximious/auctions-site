$(document).ready(function () {
	let pageTitle = $("#pageTitle").html();
	let pagMethod = "";
	switch (pageTitle) {
		case "Auctions":
			pagMethod = "Index";
			break;
		case "My auctions":
			pagMethod = "MyAuctions";
			break;
	}

	$("#auctionsPerPage").on("change", function () {
		let numOfItems = this.value;
		let index = this.selectedIndex;

		let paginationUrl =
			"https://localhost:5001/Auction/" +
			pagMethod +
			"?name=" +
			$("#auctionTitle").val() +
			"&priceLow=" +
			$("#auctionPriceLow").val() +
			"&priceHigh=" +
			$("#auctionPriceHigh").val() +
			"&auctionState=" +
			$("#auctionStatus").val() +
			"&pageSize=" +
			numOfItems;

		$.ajax({
			url: paginationUrl,
			type: "GET",
			success: function (result) {
				$("#auctions").html(result);
				$("#auctionsPerPage option[value=" + numOfItems + "]").prop(
					"selected",
					true
				);
			},
		});
	});
});
