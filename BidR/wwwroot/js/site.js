﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function showPicture() {
	let file = document.getElementById("imageInput");

	if (file && file["files"] && file["files"][0]) {
		let img = document.getElementById("auctionPicture");
		img.src = URL.createObjectURL(file.files[0]);
	}
}
