let form = $("#__AjaxAntiForgeryForm");
let token = $('input[name="__RequestVerificationToken"]', form).val();

$(document).ready(function () {
	$.ajax({
		url: "https://localhost:5001/Token/OrderList",
		type: "POST",
		data: {
			__RequestVerificationToken: token,
		},
		success: function (result) {
			let resData = [];
			let num = 1;
			console.log(result);
			result.forEach((element) => {
				let tmp = [
					"" + num++,
					"" + element.id,
					element.timestampCreated,
					"" + element.package.tokens,
					"" + element.package.price,
				];
				resData.push(tmp);
			});
			/*
			result.forEach((element) => {
				$("#tableBody").append(
					'<tr> <th scope="row">' +
						num++ +
						"</th><td>" +
						element.id +
						"</td><td>" +
						element.timestampCreated +
						"</td><td>" +
						(element.package.tokens < 10 ? "&nbsp;&nbsp;" : "") +
						element.package.tokens +
						'  <img class="tokenPng pd-2" src="assets/Token.png" alt="">' +
						"</td><td>" +
						(element.package.price < 10 ? "&nbsp;&nbsp;" : "") +
						element.package.price +
						"$</td></tr>"
				);
			});*/
			$("#myTable").DataTable({
				data: resData,
				columns: [
					{ title: "#" },
					{ title: "OrderId" },
					{ title: "Date" },
					{ title: "Tokens" },
					{ title: "Price" },
				],
			});
		},
	});
});
