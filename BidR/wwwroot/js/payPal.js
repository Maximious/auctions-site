let id = $("#packageId").val();
let price = $("#price").val();
let form = $("#__AjaxAntiForgeryForm");
let token = $('input[name="__RequestVerificationToken"]', form).val();

paypal
	.Buttons({
		createOrder: function (data, actions) {
			return actions.order.create({
				purchase_units: [
					{
						amount: {
							value: price,
						},
					},
				],
			});
		},
		onApprove: function (data, actions) {
			return actions.order.capture().then(function (details) {
				$.ajax({
					url: "https://localhost:5001/Token/BuyPackage",
					type: "POST",
					data: {
						__RequestVerificationToken: token,
						Id: id,
					},
					success: function (result) {
						console.log(result);
						if (result == "Done") {
							window.location.href = "/Token";
						}
					},
				});
			});
		},
	})
	.render("#paypal");
