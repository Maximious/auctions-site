let form = $("#__AjaxAntiForgeryForm");
let token = $('input[name="__RequestVerificationToken"]', form).val();
let status = $("#status").val();
let auctionId = $("#auctionId").val();
let res = [2];
let oldRes = [];

let func = function () {
	$.ajax({
		url: "https://localhost:5001/Auction/AuctionLicitations",
		type: "POST",
		data: {
			__RequestVerificationToken: token,
			id: auctionId,
		},
		success: function (result) {
			if (result == undefined || result.length == 0) return;
			oldRes = res;
			res = [];
			let num = 1;
			result.forEach((element) => {
				let tmp = ["" + num++, element.user.userName];
				res.push(tmp);
			});
			if (JSON.stringify(res) != JSON.stringify(oldRes)) {
				$("#myTable").dataTable().fnClearTable();
				$("#myTable").dataTable().fnAddData(res);
			}
		},
	});
};

$(document).ready(function () {
	$("#myTable").DataTable({
		data: oldRes,
		columns: [{ title: "#" }, { title: "Bidder" }],
	});
	func();
	if (status == "OPEN") setInterval(func, 10000);
});
