let auctionTitle = $("#auctionTitle");
let auctionPriceLow = $("#auctionPriceLow");
let auctionPriceHigh = $("#auctionPriceHigh");
let auctionStatus = $("#auctionStatus");
let submitButton = $("#searchButton");
let method = "";
let pageTitle = $("#pageTitle").html();
let list = $("#auctions");

$(document).ready(function () {
	$.ajax({
		url: "https://localhost:5001/Auction/AuctionStates",
		type: "GET",
		success: function (result) {
			let num = 0;
			result.forEach((element) => {
				auctionStatus.append(new Option(element, num++));
			});
			if (pageTitle == "Auctions")
				$("#auctionStatus option[value=2]").prop("selected", true);
			else {
				auctionStatus.append(new Option("ALL", 6));
				$("#auctionStatus option[value=6]").prop("selected", true);
			}
		},
	});

	submitButton.click(function () {
		let pageSize = $("#auctionsPerPage");
		let index = pageSize.val();

		switch (pageTitle) {
			case "Auctions":
				method = "Index";
				break;
			case "My auctions":
				method = "MyAuctions";
				break;
		}

		let searchUrl =
			"https://localhost:5001/Auction/" +
			method +
			"?name=" +
			auctionTitle.val() +
			"&priceLow=" +
			auctionPriceLow.val() +
			"&priceHigh=" +
			auctionPriceHigh.val() +
			"&auctionState=" +
			auctionStatus.val() +
			"&pageSize=" +
			pageSize.val();

		$.ajax({
			url: searchUrl,
			type: "GET",
			success: function (result) {
				$("#auctions").html(result);
				$("#auctionsPerPage option[value=" + index + "]").prop(
					"selected",
					true
				);
			},
		});
	});
});
