$(document).ready(function () {
	setInterval(function () {
		$(".auctionTime").each(function (index, value) {
			let time = value.firstChild.data;
			if (time === "0:0:0:0") {
				return;
			}

			let nmbrs = time.split(":");

			nmbrs[3]--;
			if (nmbrs[3] < 0) {
				nmbrs[3] = 59;
				nmbrs[2]--;
				if (nmbrs[2] < 0) {
					nmbrs[2] = 59;
					nmbrs[1]--;
					if (nmbrs[1] < 0) {
						nmbrs[1] = 23;
						nmbrs[0]--;
						if (nmbrs[0] < 0) nmbrs[0] = 0;
					}
				}
			}

			time = nmbrs.join(":");
			if (time === "0:0:0:10") {
				value.classList.add("red");
			}
			value.firstChild.data = time;
		});
	}, 1000);
});
