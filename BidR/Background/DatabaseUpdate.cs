using BidR.Models.Database;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Linq;
using System.Threading.Tasks;

[DisallowConcurrentExecution]
public class DatabaseUpdate : IJob
{
    private readonly IServiceProvider _provider;

    public DatabaseUpdate(IServiceProvider provider)
    {
        _provider = provider;
    }

    public Task Execute(IJobExecutionContext context)
    {
        using (var scope = _provider.CreateScope())
        {
            var dbContext = scope.ServiceProvider.GetService<BidRContext>();

            DateTime now = DateTime.Now;

            var openingAuctions = dbContext.auctions.Where(auction => auction.state == AuctionState.READY && auction.openedTimestamp <= now).ToList();

            foreach (var auction in openingAuctions)
            {
                auction.state = AuctionState.OPEN;
            }

            var closingAuctions = dbContext.auctions.Where(auction => auction.state == AuctionState.OPEN && auction.closedTimestamp <= now).ToList();

            foreach (var auction in closingAuctions)
            {
                auction.state = AuctionState.SOLD;
            }

            var expiringAuctions = dbContext.auctions.Where(auction => auction.state == AuctionState.DRAFT && auction.openedTimestamp <= now).ToList();

            foreach (var auction in expiringAuctions)
            {
                auction.state = AuctionState.EXPIRED;
            }

            dbContext.SaveChanges();

            return Task.CompletedTask;
        }
    }
}