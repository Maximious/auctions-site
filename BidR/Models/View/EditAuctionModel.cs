using System;
using System.ComponentModel.DataAnnotations;
using BidR.Models.Database;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BidR.Models.View
{
    public class EditAuctionModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Name")]
        [Remote(controller: "Auction", action: "CheckTitle")]
        public string name { get; set; }
        [Required]
        [Display(Name = "Description")]
        [Remote(controller: "Auction", action: "CheckDescription")]
        public string description { get; set; }
        [Display(Name = "Image")]
        [DataType(DataType.Upload)]
        public IFormFile photo { get; set; }
        [HiddenInput]
        [Display(Name = "Slika")]
        public byte[] image { get; set; }
        [Required]
        [Display(Name = "Starting price")]
        [Remote(controller: "Auction", action: "CheckStartingPrice")]
        public double startingPrice { get; set; }
        [Required]
        [Display(Name = "Created")]
        public DateTime createdTimestamp { get; set; }
        [Required]
        [Display(Name = "Opening point")]
        [Remote(controller: "Auction", action: "CheckOpenDateTime")]
        public DateTime openedTimestamp { get; set; }
        [Required]
        [Display(Name = "Closing point")]
        [Remote(controller: "Auction", action: "CheckCloseDateTime")]
        public DateTime closedTimestamp { get; set; }
        [Required]
        [Display(Name = "Increase by each bid")]
        [Remote(controller: "Auction", action: "CheckIncreasePrice")]
        public double bidPrice { get; set; }
    }
}




//[DataType(DataType.Password)]