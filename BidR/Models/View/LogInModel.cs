using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace BidR.Models.View
{
    public class LogInModel
    {
        [Required]
        [Display(Name = "Username")]
        public string username { get; set; }
        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string password { get; set; }
    }
}