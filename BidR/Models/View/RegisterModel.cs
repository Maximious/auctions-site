using System.ComponentModel.DataAnnotations;
using BidR.Controllers;
using BidR.Models.Database;
using Microsoft.AspNetCore.Mvc;

namespace BidR.Models.View
{
    public class RegisterModel
    {
        [Required]
        [Display(Name = "First name")]
        [RegularExpression(@"^[a-zA-Z''-'\s]{1,40}$",
         ErrorMessage = "Only characters and spaces allowed.")]
        public string firstName { get; set; }
        [Required]
        [Display(Name = "Last name")]
        [RegularExpression(@"^[a-zA-Z''-'\s]{1,40}$",
         ErrorMessage = "Only characters and spaces allowed.")]
        public string lastName { get; set; }
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Remote(controller: "User", action: nameof(UserController.uniqueEmail))]
        public string email { get; set; }
        [Required]
        [Display(Name = "Gender")]
        public Gender gender { get; set; }
        [Required]
        [Display(Name = "Username")]
        [Remote(controller: "User", action: nameof(UserController.correctUsername))]
        public string username { get; set; }
        [Required]
        [Remote(controller: "User", action: nameof(UserController.passwordOk))]
        [Display(Name = "Password")]

        [DataType(DataType.Password)]
        public string password { get; set; }
        [Required]
        [Display(Name = "Confirm password")]
        [Compare(nameof(password), ErrorMessage = "Passwords must match!")]
        [DataType(DataType.Password)]
        public string confirmPassword
        {
            get; set;
        }
    }
}