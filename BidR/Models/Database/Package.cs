using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;

namespace BidR.Models.Database
{
    public class Package
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }
        [Required]
        [Display(Name = "Tokens")]
        public uint tokens { get; set; }
        [Required]
        [Display(Name = "Price")]
        public double price { get; set; }

        public ICollection<Order> orders;
    }

    public class PackageConfiguration : IEntityTypeConfiguration<Package>
    {
        public void Configure(EntityTypeBuilder<Package> builder)
        {
            builder.Property(package => package.Id).ValueGeneratedOnAdd();

            builder.HasData(
                new Package()
                {
                    Id = 1,
                    name = "silver",
                    tokens = 5,
                    price = 3.2
                },
                new Package()
                {
                    Id = 2,
                    name = "gold",
                    tokens = 10,
                    price = 6.7
                },
                new Package()
                {
                    Id = 3,
                    name = "platinum",
                    tokens = 20,
                    price = 12.99
                }
            );
        }
    }
}