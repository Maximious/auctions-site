using BidR.Models.Database;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BidR.Models.Database
{
    public class BidRContext : IdentityDbContext<User>
    {
        public DbSet<Auction> auctions { get; set; }
        public DbSet<Licitation> licitations { get; set; }
        public DbSet<Package> packages { get; set; }
        public DbSet<Order> orders { get; set; }

        public BidRContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new IdentityRoleConfiguration());
            builder.ApplyConfiguration(new AuctionConfiguration());
            builder.ApplyConfiguration(new LicitationConfiguration());
            builder.ApplyConfiguration(new PackageConfiguration());
            builder.ApplyConfiguration(new OrderConfiguration());
        }
    }

}