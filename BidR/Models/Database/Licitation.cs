using System;
using System.ComponentModel.DataAnnotations;
using BidR.Models.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BidR.Models.Database
{
    public class Licitation
    {
        [Required]
        [Display(Name = "Korisnik")]
        public string UserId { get; set; }
        public User User { get; set; }

        [Required]
        [Display(Name = "Aukcija")]
        public int AuctionId { get; set; }
        public Auction Auction { get; set; }

        [Required]
        [Display(Name = "Trenutak licitacije")]
        public DateTime timestamp { get; set; }
    }

    public class LicitationConfiguration : IEntityTypeConfiguration<Licitation>
    {
        public void Configure(EntityTypeBuilder<Licitation> builder)
        {
            builder.HasKey(
                entity => new { entity.UserId, entity.AuctionId, entity.timestamp }
            );
        }
    }
}