using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using BidR.Models.Database;
using BidR.Models.View;
using AutoMapper;

namespace BidR.Models.Database
{

    public enum AuctionState
    {
        DRAFT = 0,
        READY = 1,
        OPEN = 2,
        SOLD = 3,
        EXPIRED = 4,
        DELETED = 5
    }

    public class Auction
    {
        public Auction()
        {
            state = AuctionState.DRAFT;
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Name")]
        //[Remote(controller: "Auction", action: "CheckTitle")]
        public string name { get; set; }
        [Required]
        [Display(Name = "Description")]
        //[Remote(controller: "Auction", action: "CheckDescription")]
        public string description { get; set; }
        [Required]
        [Display(Name = "Image")]
        //[Remote(controller: "Auction", action: "CheckPicture")]
        public byte[] image { get; set; }
        [Required]
        [Display(Name = "Starting price")]
        //[Remote(controller: "Auction", action: "CheckStartingPrice")]
        public double startingPrice { get; set; }
        [Required]
        [Display(Name = "Creating point")]
        //[Remote(controller: "Auction", action: "CheckCreateDateTime")]
        public DateTime createdTimestamp { get; set; }
        [Required]
        [Display(Name = "Opening point")]
        //[Remote(controller: "Auction", action: "CheckOpenDateTime")]
        public DateTime openedTimestamp { get; set; }
        [Required]
        [Display(Name = "Closing point")]
        //[Remote(controller: "Auction", action: "CheckCloseDateTime")]
        public DateTime closedTimestamp { get; set; }
        [Required]
        [Display(Name = "State")]
        public AuctionState state { get; set; }
        [Required]
        [Display(Name = "Increase by each bid")]
        //[Remote(controller: "Auction", action: "CheckIncreasePrice")]
        public double bidPrice { get; set; }
        [Display(Name = "New price")]
        public double newPrice { get; set; }
        [Required]
        [Display(Name = "Owner")]
        public string UserId { get; set; }
        public User User { get; set; }
        [Display(Name = "Winner")]
        public string WinnerId { get; set; }
        public User Winner { get; set; }

        public ICollection<Licitation> licitations;

        public static String[] getStatuses()
        {
            return Enum.GetNames(typeof(AuctionState));
        }

        //konkurentnost
        [Timestamp]
        public byte[] RowVersion { get; set; }
        /*
        public IList<SelectListItem> getStatuses()
        {
            bool[] activeSates = { false, false, false, false, false, false };
            activeSates[(int)state] = true;
            SelectListItem draft = new SelectListItem("DRAFT", AuctionState.DRAFT.ToString(), activeSates[0]);
            SelectListItem ready = new SelectListItem("DRAFT", AuctionState.READY.ToString(), activeSates[1]);
            SelectListItem open = new SelectListItem("DRAFT", AuctionState.OPEN.ToString(), activeSates[2]);
            SelectListItem sold = new SelectListItem("DRAFT", AuctionState.SOLD.ToString(), activeSates[3]);
            SelectListItem expired = new SelectListItem("DRAFT", AuctionState.EXPIRED.ToString(), activeSates[4]);
            SelectListItem deleted = new SelectListItem("DRAFT", AuctionState.DELETED.ToString(), activeSates[5]);

            IList<SelectListItem> states = new List<SelectListItem>();

            states.Add(draft);
            states.Add(ready);
            states.Add(open);
            states.Add(sold);
            states.Add(expired);
            states.Add(deleted);

            return states;
        }*/
    }

    public class AuctionConfiguration : IEntityTypeConfiguration<Auction>
    {
        public void Configure(EntityTypeBuilder<Auction> builder)
        {
            builder.Property(auction => auction.Id).ValueGeneratedOnAdd();
        }
    }

    public class AuctionProfile : Profile
    {
        public AuctionProfile()
        {
            base.CreateMap<AuctionModel, Auction>()
                /*ForMember(
                    destination => destination.Email,
                    options => options.MapFrom(data => data.email)
                ).
                ForMember(
                    destination => destination.UserName,
                    options => options.MapFrom(data => data.username)
                )*/;
        }
    }
}
