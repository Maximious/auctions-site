using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using BidR.Models.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BidR.Models.Database
{
    public class Order
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "User")]
        public string UserId { get; set; }
        public User User { get; set; }

        [Required]
        [Display(Name = "Created")]
        public DateTime timestampCreated { get; set; }

        [Required]
        [Display(Name = "Package")]
        public int PackageId { get; set; }
        public Package Package { get; set; }
    }

    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.Property(order => order.Id).ValueGeneratedOnAdd();
        }
    }
}