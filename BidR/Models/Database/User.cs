using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AutoMapper;
using BidR.Models.View;
using Microsoft.AspNetCore.Identity;

namespace BidR.Models.Database
{
    public enum Gender
    {
        Male = 0,
        Female = 1
    }

    public class User : IdentityUser
    {
        [Required]
        public string firstName { get; set; }
        [Required]
        public string lastName { get; set; }
        [Required]
        public Gender gender { get; set; }
        [Required]
        public bool deleted { get; set; }
        [Required]
        public uint tokens { get; set; }

        //konkurentnost
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public ICollection<Order> orders;
        public ICollection<Auction> auctions;
        public ICollection<Licitation> licitations;
    }

    public class UserProfile : Profile
    {
        public UserProfile()
        {
            base.CreateMap<RegisterModel, User>().
                ForMember(
                    destination => destination.Email,
                    options => options.MapFrom(data => data.email)
                ).
                ForMember(
                    destination => destination.UserName,
                    options => options.MapFrom(data => data.username)
                );
        }
    }

}