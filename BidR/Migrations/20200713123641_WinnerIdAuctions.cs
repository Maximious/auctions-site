﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BidR.Migrations
{
    public partial class WinnerIdAuctions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "WinnerId",
                table: "auctions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_auctions_WinnerId",
                table: "auctions",
                column: "WinnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_auctions_AspNetUsers_WinnerId",
                table: "auctions",
                column: "WinnerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropIndex(
                name: "IX_auctions_WinnerId",
                table: "auctions");

            migrationBuilder.DropColumn(
                name: "WinnerId",
                table: "auctions");
        }
    }
}
