﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BidR.Migrations
{
    public partial class TokensAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "tokens",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 0);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropColumn(
                name: "tokens",
                table: "AspNetUsers");
        }
    }
}
