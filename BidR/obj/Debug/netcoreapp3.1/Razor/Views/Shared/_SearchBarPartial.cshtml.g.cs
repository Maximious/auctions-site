#pragma checksum "/home/vol/SestiSemestar/UcenjeNaDaljinu/IEP/iep-auctions-site/BidR/Views/Shared/_SearchBarPartial.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "52c59d4eb29a23008bde5a0393a6ba50cda3c2b1"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__SearchBarPartial), @"mvc.1.0.view", @"/Views/Shared/_SearchBarPartial.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "/home/vol/SestiSemestar/UcenjeNaDaljinu/IEP/iep-auctions-site/BidR/Views/_ViewImports.cshtml"
using BidR;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/home/vol/SestiSemestar/UcenjeNaDaljinu/IEP/iep-auctions-site/BidR/Views/_ViewImports.cshtml"
using BidR.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"52c59d4eb29a23008bde5a0393a6ba50cda3c2b1", @"/Views/Shared/_SearchBarPartial.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ed0f9999beec5c0674b43265211890912b6fa0e1", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__SearchBarPartial : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<div class=""container-fluid"">
    <div class=""row mb-4 d-flex justify-content-center"">
        <div class=""col-6 d-flex justify-content-center"">
            <b>Search</b>
        </div>
    </div>
    <div class=""row d-flex justify-content-center"">
        <div class=""col-6 d-flex justify-content-center"">
            Name
        </div>
    </div>
    <div class=""row d-flex justify-content-center"">
        <div class=""col mt-2 mb-3 d-flex justify-content-center"">
            <input type=""text"" id=""auctionTitle"" />
        </div>
    </div>
    <div class=""row d-flex justify-content-center"">
        <div class=""col-6 d-flex justify-content-center"">
            Price
        </div>
    </div>
    <div class=""row d-flex justify-content-center"">
        <div class=""col-5 d-flex justify-content-center"">
            Low:
        </div>
        <div class=""col-5 d-flex justify-content-center"" >
            High:
        </div>
    </div>
    <div class=""row"">
        <div class=""col-6 mt-2 mb-3 pr-1"">
            <i");
            WriteLiteral(@"nput type=""number"" id=""auctionPriceLow"" />
        </div>
        <div class=""col-6 mt-2 mb-3 pl-1"">
            <input type=""number"" id=""auctionPriceHigh"" />
        </div>
    </div>
    <div class=""row d-flex justify-content-center"">
        <div class=""col-6 d-flex justify-content-center"">
            Status
        </div>
    </div>
    <div class=""row d-flex justify-content-center"">
        <div class=""col-6 mt-2 mb-3 d-flex justify-content-center"">
            <select name=""status"" id=""auctionStatus"">
            </select>
        </div>
    </div>
    <div class=""row d-flex justify-content-center"">
        <div class=""col-6 mt-2 mb-3 d-flex justify-content-center"">
            <button id=""searchButton"" type=""button"" class=""btn btn-success"">SEARCH</button>
        </div>
    </div>
</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
